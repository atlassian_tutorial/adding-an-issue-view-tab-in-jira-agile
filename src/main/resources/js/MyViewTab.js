AJS.$(function () {
    // the API event for saying the board is updated
    var installButtonHandlers = function() {

        AJS.$('button.pulpfiction').bind('click', function () {
            // ok cause the board to reload via another API event
            JIRA.trigger('GH.RapidBoard.causeBoardReload');
        });
    };
    JIRA.bind('GH.DetailView.updated', installButtonHandlers)
});
