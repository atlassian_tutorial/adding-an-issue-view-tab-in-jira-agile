# Tutorial: Adding a Detail View Tab

This sample plugin demonstrates how to add a custom tab to the issue details view in 
the Atlassian Jira Agile board. For a full discussion of this sample, see the tutorial at: 
[Tutorial: Adding a Detail View Tab][1].

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

    atlas-mvn jira:run

 [1]: https://developer.atlassian.com/jiradev/jira-applications/jira-software/tutorial-adding-a-detail-view-tab
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project